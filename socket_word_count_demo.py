from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
import pyspark.sql.functions as f

def remove_stopwords(s):
    stopwords = ["!", "?", ".", ",", ";", ":", "'", '"']
    for w in stopwords:
        s = s.replace(w, " ")
    stopwords = ["A", "a", "an", "An", "the", "The"]
    for w in stopwords:
        s = s.replace(" {w} ", " ")
    return s.strip()

remove_stopwords_udf = udf(remove_stopwords)


conf = SparkConf().setAppName("PySpark App").setMaster("local[*]")
sc = SparkContext(conf=conf)
spark = SparkSession(sc)
    

df = spark.readStream\
        .format("socket")\
        .option("host","localhost")\
        .option("port", 9999)\
        .option("includeTimestamp", True)\
        .load()

df = df.withColumn("text", remove_stopwords_udf("value"))
df.printSchema()

words = df.select(f.explode(f.split(df["text"],"\W+")).alias("word"), "timestamp")


counts = words\
    .groupBy(f.window("timestamp", "10 seconds", "5 seconds"), "word")\
    .count()

counts.printSchema()

query = counts.writeStream\
    .format("console")\
    .outputMode("complete")

query.start().awaitTermination()
